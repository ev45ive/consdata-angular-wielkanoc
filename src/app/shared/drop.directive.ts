import { Directive, HostListener, EventEmitter, Output, HostBinding } from '@angular/core';

@Directive({
  selector: '[drop]'
})
export class DropDirective {

  constructor() { }

  @Output('modelDrop')
  dropEmitter = new EventEmitter()

  @HostBinding('class.table-success')
  hover = false

  @HostListener('dragover',["$event.dataTransfer"])
  acceptDrag(dt:DataTransfer){
    if([...dt.types].includes('application/x-music-playlist-id')){
      this.hover = true
      return false
    }
  }

  @HostListener('dragleave')
  endDrag(){
    this.hover = false
  }

  @HostListener('drop',["$event.dataTransfer"])
  acceptDrop(dt:DataTransfer){
    this.hover = false
    const data = dt.getData('application/x-music-playlist-id')
    this.dropEmitter.emit(data)
  }

}
