import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightDirective } from './highlight.directive';
import { DragDirective } from './drag.directive';
import { DropDirective } from './drop.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [HighlightDirective, DragDirective, DropDirective],
  exports: [HighlightDirective, DragDirective, DropDirective]
})
export class SharedModule { }
