import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[drag]'
})
export class DragDirective {

  @HostBinding('draggable')
  draggable = true

  @Input('drag')
  dragModel


  @HostListener('dragstart',["$event.dataTransfer"])
  acceptDrag(dt:DataTransfer){
    dt.setData('text/plain','Playlist #URL')
    dt.setData('application/x-music-playlist-id', this.dragModel)
  }


  constructor() { 
    // console.log('drag')
  }

}
