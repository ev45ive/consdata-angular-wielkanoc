import { Directive, ElementRef, Input, OnInit, HostListener, HostBinding} from '@angular/core';

@Directive({
  selector: '[highlight]',
  // host:{
  //   '(click)':'clicked($event)'
  //    '[style.color]':"active? color : 'initial'"
  // }
})
export class HighlightDirective implements OnInit{

  @Input('highlight')
  set highlight(color){
    this.color = color
  }

  color
  
  active = false
  
  @HostBinding('style.color')
  get currentColor(){
    // console.log('puk puk, jaki kolor?')
    return this.active? this.color : 'initial'
  }

  @HostListener('mouseenter'/* ,['$event.x','$event.y'] */)
  onEnter(/* x:number, y:number */){
    // this.elem.nativeElement.style.color = this.color 
    this.active = true   
  }

  @HostListener('mouseleave')
  onLeave(){
    // this.elem.nativeElement.style.color = 'initial'  
    this.active = false
  }

  constructor(private elem: ElementRef) {

  }

  ngOnInit() {
    // this.elem.nativeElement.style.color = this.color  
  }
}
