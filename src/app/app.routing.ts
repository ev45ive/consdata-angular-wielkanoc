import { RouterModule, Routes } from '@angular/router'

const routes: Routes = [
  {
    path: '',
    redirectTo: 'playlists',
    pathMatch: 'full'
  },
  {
    path: 'music',
    loadChildren:'app/music/music.module#MusicModule'
  },
  {
    path:'**',
    redirectTo: 'music',
    pathMatch:'full'
  }
]

export const Routing = RouterModule.forRoot(routes, {
  // enableTracing: true,
  // useHash: true
})