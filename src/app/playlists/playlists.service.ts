import { Injectable } from '@angular/core';
import { Playlist } from '../models/playlist';
import { of } from 'rxjs/observable/of'
import { delay, map, tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PlaylistsService {

  save(playlist:Playlist){
    this.http.put('http://localhost:3000/playlists/'+playlist.id, playlist)
    .subscribe(()=>{
      this.getPlaylists()
    })

    // const found = this.playlists$.getValue().findIndex(
    //   p => p.id == playlist.id
    // )

    // const playlists = [...this.playlists$.getValue()]
    // playlists.splice(found,1,playlist)
    // this.playlists$.next(playlists)
  }

  constructor(private http:HttpClient) {

  }

  getPlaylists(){
    this.http.get('http://localhost:3000/playlists').subscribe(data => {
      this.playlists$.next(<Playlist[]>data)
    })
    return this.playlists$.asObservable()
  }

  getPlaylist(id){
    return this.playlists$.pipe(
      // tap(console.log),
      map(playlists => playlists.find(
        playlist => playlist.id == id
      ))
    )
  }

  playlists$ = new BehaviorSubject<Playlist[]>([
    {
      id: 123,
      name: 'Angular Greatest Hits',
      favourite: false,
      color: '#ff0000'
    }, {
      id: 234,
      name: 'Angular TOP 20!',
      favourite: false,
      color: '#00ff00'
    }, {
      id: 35,
      name: 'The best of Angular',
      favourite: false,
      color: '#0000ff'
    }
  ])
}
