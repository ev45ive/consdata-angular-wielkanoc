import { Component, OnInit } from '@angular/core';
import { Playlist } from '../models/playlist';
import { PlaylistsService } from './playlists.service';
import { Router, ActivatedRoute } from '@angular/router';
import { pluck, tap } from 'rxjs/operators';

@Component({
  selector: 'playlists',
  template: `
    <!-- .row>.col*2 -->

    <div class="row">
      <div class="col">
     <!--  <items-list 
        [items]="playlists"
        [(selected)]="selected">
        </items-list> -->

        <items-list 
          [items]="playlists$ | async"
          [selected]="selected$ | async"
          (selectedChange)="selectById($event)">
          </items-list>
      </div>
      <div class="col" 
        drop (modelDrop)="selectById($event)">
  

        <router-outlet></router-outlet>
      </div>
    </div>

  `,
  styles: []
})
export class PlaylistsComponent implements OnInit {

  playlists$ = this.service.getPlaylists()
  selected$ = this.route.firstChild.params.pipe(
    // tap(console.log),
    pluck('id')
  )

  selectById(id) {
    this.router.navigate(['playlists',id])
    // this.router.events
  }

  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private service: PlaylistsService) { }

  ngOnInit() {
  }

}
