import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from './playlists.service';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap, pluck } from 'rxjs/operators';
import { Playlist } from '../models/playlist';

@Component({
  selector: 'playlist-container',
  template: `
    <playlist-details  
      *ngIf="selected$ | async as playlist; else elseRef"
      (save)="save($event)"
      [playlist]="playlist">
    </playlist-details>
  
    <ng-template #elseRef>
      Please select playlist
    </ng-template>
  `,
  styles: []
})
export class PlaylistContainerComponent implements OnInit {

  selected$ = this.route.paramMap
  .pipe(
    map(params => params.get('id')),
    switchMap(id => this.service.getPlaylist(id))
  )

  // selected$ = this.route.data.pipe(
  //   pluck<{},Playlist>('playlist')
  // )
  
  save(playlist){
    this.service.save(playlist)
  }

  constructor(
    private route:ActivatedRoute,
    private service: PlaylistsService
  ) { }
    
    ngOnInit() {
    // const id = this.route.snapshot.paramMap.get('id')
  }

}
