import { Component, OnInit, ViewEncapsulation, Input, EventEmitter, Output } from '@angular/core';
import { Playlist } from '../models/playlist';

interface Item {
  id: number | string;
  name:string
  color: string
}

// (mouseenter)="hover = item"
// (mouseleave)="hover = null" 
// [style.color]="(hover == item? item.color : 'initial')"


@Component({
  selector: 'items-list',
  template: `
    <div class="list-group">
      <div class="list-group-item colored" 
        *ngFor="let item of items trackBy itemIndexFn; index as i"
        [style.borderLeftColor]="item.color"

        [drag]="item.id"
        
        [highlight]=" item.color "

        [class.active]="selected == item.id"
        (click)="select(item)">
        {{i+1}}.  {{item.name}} 
      </div>
    </div>
    {{hover?.name}}
  `,
  styles: [`
    .colored{
      border-left:10px solid;
    }
    :host(.bordered){
      border: 1px solid black;
      display: block;
    }
    :host-context(.colored) ::ng-deep p {
      color: hotpink;
    }
  `]
})
export class ItemsListComponent<T extends Item> implements OnInit {

  hover:T

  @Input()
  selected: string | number

  @Output()
  selectedChange = new EventEmitter<string | number>()

  @Input('items')
  items: T[]

  select(item:T){
    this.selectedChange.emit(item.id)
  }

  itemIndexFn(item){
    return item.id
  }

  constructor() { }

  ngOnInit() {
  }

}
