import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaylistsComponent } from './playlists.component';
import { ItemsListComponent } from './items-list.component';
import { ListItemComponent } from './list-item.component';
import { PlaylistDetailsComponent } from './playlist-details.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module'
import { Routing, ResolvePlaylist } from './playlists.routing';
import { PlaylistContainerComponent } from './playlist-container.component';
import { PlaylistsService } from './playlists.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    Routing,
    HttpClientModule
  ],
  declarations: [
    PlaylistsComponent, 
    ItemsListComponent, 
    ListItemComponent, 
    PlaylistDetailsComponent, PlaylistContainerComponent
  ],
  exports:[
    PlaylistsComponent
  ],
  providers: [
    PlaylistsService,
    ResolvePlaylist,
  ]
})
export class PlaylistsModule { }
