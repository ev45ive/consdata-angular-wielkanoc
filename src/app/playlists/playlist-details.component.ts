import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Playlist } from '../models/playlist';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'playlist-details',
  template: `
    <div [ngSwitch]="mode">

      <div *ngSwitchDefault>
        <div class="form-group">
          <label>Name</label>
          <div>{{ playlist.name }}</div>
        </div>
        <div class="form-group">
          <label>Favourite</label>
          <div>{{ playlist.favourite?  'Yes' : 'No' }}</div>
        </div>
        <div class="form-group">
          <label>Color</label>
          <div [style.color]="playlist.color" >
            {{ playlist.color }}
          </div>
        </div>
        <button class="btn btn-info" (click)="edit()">Edit</button>
      </div>
  
      <form #formRef="ngForm" *ngSwitchCase=" 'edit' " (ngSubmit)="save(formRef)">
        <div class="form-group">
          <label>Name</label>
          <input type="text" class="form-control" [ngModel]="playlist.name" name="name">
        </div>
        <div class="form-group">
          <label>Favourite</label>
          <input type="checkbox" [ngModel]="playlist.favourite" name="favourite">
        </div>
        <div class="form-group">
          <label>Color</label>
          <input type="color" [(ngModel)]="playlist.color" name="color">
        </div>
        <button class="btn btn-danger" (click)="cancel()">Cancel</button>
        <button class="btn btn-success">Save</button>
      </form>

    </div>
  `,
  styles: [],
  // inputs:[
  //   'playlist:playlist'
  // ]
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist:Playlist

  @Output('save')
  saveEmitter = new EventEmitter<Playlist>()

  save(form:NgForm){
    // console.log(form)
    this.saveEmitter.emit({
      ...this.playlist,
      ...form.value as Partial<Playlist>
    })
  }

  mode = 'show' // 'edit'

  edit(){
    this.mode = 'edit'
  }

  cancel(){
    this.mode = 'show'
  }

  constructor() { }

  ngOnInit() {
  }

}
