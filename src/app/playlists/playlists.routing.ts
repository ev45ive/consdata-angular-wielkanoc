import { RouterModule, Routes, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { PlaylistsComponent } from './playlists.component';
import { PlaylistContainerComponent } from './playlist-container.component';
import { Playlist } from '../models/playlist';
import { PlaylistsService } from './playlists.service';
import { Injectable } from '@angular/core';

@Injectable()
export class ResolvePlaylist implements Resolve<Playlist> {
  constructor(private service: PlaylistsService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.service.getPlaylist(route.paramMap.get('id'))
  }
}

const routes: Routes = [
  {
    path: 'playlists',
    component: PlaylistsComponent,
    children: [
      {
        path: '',
        component: PlaylistContainerComponent
      },
      {
        path: ':id',
        component: PlaylistContainerComponent,
        resolve: {
          // 'playlist':ResolvePlaylist
        }
      }
    ]
  },
]

export const Routing = RouterModule.forChild(routes)