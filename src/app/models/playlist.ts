export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  color: string;
  /**
   * Songs, etc..
   */
  tracks?: Track[]
}

export interface Track{
  id: number;
  title: string;
}