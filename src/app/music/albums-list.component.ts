import { Component, OnInit, Input } from '@angular/core';
import { Album } from '../models/album';

@Component({
  selector: 'albums-list',
  template: `
    <div class="card-group">
      <album-item class="card" 
                  [album]="album"
                  *ngFor="let album of albums">
      </album-item>
   </div>
  `,
  styles: [`
    .card{
      flex: 0 0 25%;
    }
  `]
})
export class AlbumsListComponent implements OnInit {

  @Input()
  albums: Album[]

  constructor() { }

  ngOnInit() {
  }

}
