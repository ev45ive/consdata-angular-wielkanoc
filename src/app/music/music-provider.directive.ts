import { Directive } from '@angular/core';
import { MusicService } from './music.service';

@Directive({
  selector: '[appMusicProvider]',
  providers:[
    MusicService
  ]
})
export class MusicProviderDirective {

  constructor() { }

}
