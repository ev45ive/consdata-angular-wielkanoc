import { Component, OnInit, Input } from '@angular/core';
import { Album } from '../models/album';

@Component({
  selector: 'album-item',
  template: `
    <img class="card-img-top"  
         [src]="album.images[0].url">
    <div class="card-body">
      <h5 class="card-title">{{album.name}}</h5>
    </div>
  `,
  styles: []
})
export class AlbumItemComponent implements OnInit {

  @Input()
  album:Album

  constructor() { }

  ngOnInit() {
  }

}
