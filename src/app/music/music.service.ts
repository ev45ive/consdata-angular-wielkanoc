import { Injectable, Inject, InjectionToken } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { AuthService } from '../auth/auth.service';
import { Album } from '../models/album';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { of } from 'rxjs/observable/of'

// interface AlbumsResponse{
//   albums:{
//     items: Album[]
//   } 
//  }

type ServerResponse<name extends string, T> = {
  [k in name]: {
    items: T[]
  }
}
type AlbumsResponse = ServerResponse<'albums', Album>

// import 'rxjs/Rx'
// import 'rxjs/add/operator/audit'
import { map, tap, startWith, switchMap, filter } from 'rxjs/operators'
import { Artist } from '../models/artist';

export const URL_TOKEN = new InjectionToken<string>('URL for music search service')

@Injectable()
export class MusicService {

  albums$ = new BehaviorSubject<Album[]>([])
  queries$ = new BehaviorSubject<string>('')

  constructor(
    @Inject(URL_TOKEN) private url,
    private http: HttpClient
  ) {
    this.queries$
      .pipe(
        filter(query => query.length >= 1),
        switchMap(query => this.http.get<AlbumsResponse>(this.url, {
          params: {
            q: query,
            type: 'album'
          }
        })),
        tap(console.log),
        map(response => response.albums.items),
      )
      .subscribe(albums => {
        this.albums$.next(albums)
      })
  }

  search(query) {
    this.queries$.next(query)
  }

  albums = []

  getAlbums(query = 'batman') {
    return this.albums$.asObservable()
  }

}
