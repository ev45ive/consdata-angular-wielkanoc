import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumsListComponent } from './albums-list.component';
import { AlbumItemComponent } from './album-item.component';
import { environment } from '../../environments/environment';
import { MusicService, URL_TOKEN } from './music.service';
import { HttpClientModule, HttpClient, HttpHandler } from '@angular/common/http';
import { AuthModule } from '../auth/auth.module';
import { MusicProviderDirective } from './music-provider.directive';
import { ReactiveFormsModule } from '@angular/forms'
import { Routing } from './music.routing';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    AuthModule.forChild(),
    ReactiveFormsModule,
    Routing
  ],
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    AlbumsListComponent,
    AlbumItemComponent,
    MusicProviderDirective
  ],
  exports: [
    // MusicSearchComponent, 
    // MusicProviderDirective
  ],
  providers:[
    {
      provide: URL_TOKEN,
      useValue: environment.MUSIC_SEARCH_URL
    },
    /* {
      provide:HttpClient,
      useFactory: (handler)=>{
        return new HttpClient(handler)
      },deps:[HttpHandler]
    } */
    /* {
      provide: 'MusicService',
      useFactory: (url) => {
        return new MusicService(url)
      },
      deps:['MUSIC_SEARCH_URL']
    }, */
    /* {
      provide: AbstractMusicService,
      useClass: ConcreteMusicService
    }, */
    /* {
      provide: MusicService,
      useClass: MusicService
    }, */
    MusicService,
  ]
  })
  export class MusicModule { }
