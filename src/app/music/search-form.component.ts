import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { filter, distinctUntilChanged, debounceTime, withLatestFrom } from 'rxjs/operators';
import { AbstractControl, FormControl, FormGroup, FormArray, FormBuilder, Validators, ValidatorFn, ValidationErrors, AsyncValidatorFn, AsyncValidator } from '@angular/forms'
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

@Component({
  selector: 'search-form',
  template: `
  <div class="form-group my-3" [formGroup]="queryForm">
    <input type="text" class="form-control" 
        placeholder="Search albums" formControlName="query">
        <ng-container *ngIf="queryForm.get('query').touched || queryForm.get('query').dirty">
          <div *ngIf="queryForm.get('query').hasError('required')">
            Field is required
          </div>
          <div *ngIf="queryForm.get('query').getError('minlength') as error">
            Minimum length is {{error.requiredLength}}
          </div>
          <div *ngIf="queryForm.get('query').getError('censor') as error">
            Can't use words like {{error.badword}}
          </div>
        </ng-container>
        <div *ngIf="queryForm.pending">Please be patient... </div>
  </div>
  `,
  styles: [`
    .form-group .ng-invalid.ng-touched,
    .form-group .ng-invalid.ng-dirty {
      border: 2px solid red;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  @Input()
  set query(query) {
    const f = this.queryForm.get('query') as FormControl
    f.setValue(query, { emitEvent: false, onlySelf: true })
  }

  queryForm: FormGroup

  constructor(private fb: FormBuilder) {

    const censor = (badword): ValidatorFn => (c: AbstractControl): ValidationErrors | null => {
      const hasError = c.value.includes(badword)

      return hasError ? {
        'censor': { badword: badword }
      } : null
    }

    const asyncCensor = (badword): AsyncValidatorFn => (c: AbstractControl): Observable<ValidationErrors | null> => {
      // this.http.get('/validation,...).pipe(map( response => validation_errors || null ))
      const checkErrors = censor(badword)

      return Observable.create((observer: Observer<ValidationErrors | null>) => {
        window.setTimeout(() => {
          const errors = checkErrors(c)

          observer.next(errors)
          observer.complete()
        }, 2000)
      })
    }

    this.queryForm = this.fb.group({
      'query': this.fb.control('', [
        Validators.required,
        Validators.minLength(3),
        // censor('batman')
      ], [
          asyncCensor('batman')
        ])
    })

    console.log(this.queryForm)

    const valid$ = this.queryForm.statusChanges.pipe(
      filter(s => s === 'VALID')
    )
    const value$ = this.queryForm.get('query')
      .valueChanges
      .pipe(
        debounceTime(400),
        // filter(query => query.length >= 3),
        distinctUntilChanged(),
    )

    this.queryChange = valid$.pipe(
      withLatestFrom(value$, (valid, value) => value)
    )
  }

  ngOnInit() {
  }

  @Output()
  queryChange: Observable<string>

}
