import { Component, OnInit, Inject } from '@angular/core';
import { MusicService } from './music.service';
import { Album } from '../models/album';
import { Subscription } from 'rxjs/Subscription';


@Component({
  selector: 'music-search',
  template: `
    <div class="row">
      <div class="col">
        <search-form (queryChange)="search($event)" 
                      [query]="queries$ | async">
        </search-form>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <albums-list [albums]="albums$ | async"></albums-list>
      </div>
    </div>
  `,
  // viewProviders: [
  //   MusicService
  // ],
  styles: []
})
export class MusicSearchComponent implements OnInit {

  albums$ = this.musicService.getAlbums()
  queries$ = this.musicService.queries$

  constructor(private musicService: MusicService) { }

  search(query) {
    this.musicService.search(query)
  }

  ngOnInit() {
  }

}
