import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

@Injectable()
export class AuthService {

  constructor() { }

  token: string

  authorize() {
    const url = 'https://accounts.spotify.com/authorize'

    const params = new HttpParams({
      fromObject: {
        client_id: '15bd0a07c81049c7ac7becc577f4549c',
        response_type: 'token',
        redirect_uri: 'http://localhost:4200/'
      }
    })
    sessionStorage.removeItem('token')
    window.location.replace(`${url}?${params.toString()}`)
  }

  getToken() {
    this.token = JSON.parse(sessionStorage.getItem('token'))

    if(!this.token){
      const params = new HttpParams({
        fromString: window.location.hash.substring(1)
      })
      this.token = params.get('access_token')
      sessionStorage.setItem('token', JSON.stringify(this.token))
      window.location.hash = ''
    }

    if (!this.token) {
      this.authorize()
    }
    return this.token
  }

}
