import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from './auth.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptorService } from './auth-interceptor.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptorService,
    multi: true,
  }],
})
export class AuthModule {

  static forChild(): ModuleWithProviders {
    return {
      ngModule: AuthModule,
      providers: []
    }
  }

  static forRoot(config?): ModuleWithProviders {
    return {
      ngModule: AuthModule,
      providers: [
        AuthService,
        {
          provide: 'AUTH_CONFIG',
          useValue: {
            tokenName: 'access_token',
            client_id: config.client_id
          }
        },
      ]
    }
  }

  constructor(private auth: AuthService) {
    this.auth.getToken()
  }
}
