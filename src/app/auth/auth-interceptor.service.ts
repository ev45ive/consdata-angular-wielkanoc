import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpResponse, HttpEvent, HTTP_INTERCEPTORS, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { catchError, tap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const authenticatedRequest = req.clone({
      setHeaders:{
        'Authorization': `Bearer ${this.auth.getToken()}`
      },
    })
    return next.handle(authenticatedRequest)
      .pipe(
        catchError((err, caught) => {
          if(err instanceof HttpErrorResponse && err.status == 401){
              this.auth.authorize()
              // debugger;
              // return caught
          }
          return []
        })
      )
  }

  constructor(private auth: AuthService) { }

}
