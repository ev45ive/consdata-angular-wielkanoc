import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { MusicModule } from './music/music.module';
import { AuthModule } from './auth/auth.module';
import { Routing } from './app.routing';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PlaylistsModule,
    // MusicModule,
    Routing,
    AuthModule.forRoot({
      client_id:'placki'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

  // ngDoBootstrap(){

  // }
 }
