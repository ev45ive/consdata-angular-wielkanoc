export const environment = {
  production: true,

  MUSIC_SEARCH_URL: 'https://api.spotify.com/v1/search'
};
